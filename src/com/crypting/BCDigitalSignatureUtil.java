package com.crypting;

import org.apache.log4j.Logger;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.bcpg.BCPGOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.*;
import org.bouncycastle.openpgp.jcajce.JcaPGPObjectFactory;
import org.bouncycastle.openpgp.operator.jcajce.*;

import java.io.*;
import java.nio.file.Files;
import java.security.Security;
import java.util.Iterator;

//TODO 1. probably can be optimized by creating common bouncycastle objects which can be shared and reused by this util
// methods but it would require testing which objects are reusable because for example PGPLiteralDataGenerator requires
// to be used only for one crypting operation - see decompiled version of PGPLiteralDataGenerator.class:48
// 2. Also should check under profiler or timestamps comparison which operations are most resource consuming -
// crypting itself or bouncycastle objects instantiation
/**
 * based on org.bouncycastle.openpgp.examples.SignedFileProcessor
 */
public class BCDigitalSignatureUtil {
    static final Logger log = Logger.getLogger(BCDigitalSignatureUtil.class);

    //generate unique temporary file name to avoid conflicts of multiple requests at the same time
    private static String generateTemporaryFileName(String filenameStart) {
        return (filenameStart + System.currentTimeMillis()) + System.nanoTime() + ".tmp";
    }

    public static void deleteTemporaryFiles() {
        File f = new File("."); // current directory

        File[] files = f.listFiles();
        for (File file : files) {
            if (file.isFile() && file.getName().contains(".tmp") && file.getName().contains("outputfile")) {
                log.info(file.getName());
                file.delete();
            }
        }
    }

    /**
     * @param pgpSecretKey - can be fetched by com.crypting.BCKeyPairUtil.readSecretKey
     * @param inputFileName - name of file for signing
     * @param outputFileName - name of signed file for output
     * @param passPhrase - the phrase used on key exporting by com.crypting.BCKeyPairUtil#exportKeyPairLocally
     * @param asciiArmored - define if to apply binary-to-text encoding for signed file content -
     *      *      *              https://en.wikipedia.org/wiki/Binary-to-text_encoding
     * @return outputFile created on file system with signed file content
     * @throws IOException
     * @throws PGPException
     */
    public static File signFile(PGPSecretKey pgpSecretKey,
                                String inputFileName,
                                String outputFileName,
                                String passPhrase,
                                boolean asciiArmored) throws IOException, PGPException {
        Security.addProvider(new BouncyCastleProvider());

        PGPPrivateKey pgpPrivateKey = pgpSecretKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder()
                .setProvider("BC").build(passPhrase.toCharArray()));
        PGPSignatureGenerator pgpSignatureGenerator = new PGPSignatureGenerator(new JcaPGPContentSignerBuilder(
                pgpSecretKey.getPublicKey().getAlgorithm(),
                PGPUtil.SHA1)
                .setProvider("BC"));
        pgpSignatureGenerator.init(PGPSignature.BINARY_DOCUMENT, pgpPrivateKey);

        Iterator publicKeysIterator = pgpSecretKey.getPublicKey().getUserIDs();
        if(publicKeysIterator.hasNext()) {
            PGPSignatureSubpacketGenerator pgpSignatureSubpacketGenerator = new PGPSignatureSubpacketGenerator();
            pgpSignatureSubpacketGenerator.setSignerUserID(false, (String)publicKeysIterator.next());
            pgpSignatureGenerator.setHashedSubpackets(pgpSignatureSubpacketGenerator.generate());
        }

        PGPCompressedDataGenerator pgpCompressedDataGenerator = new PGPCompressedDataGenerator(PGPCompressedData.ZLIB);
        File outputFile = new File(outputFileName);
        OutputStream outputStream = new FileOutputStream(outputFile);
        if(asciiArmored) {
            outputStream = new ArmoredOutputStream(outputStream);
        }
        BCPGOutputStream bcpgOutputStream = new BCPGOutputStream(pgpCompressedDataGenerator.open(outputStream));
        pgpSignatureGenerator.generateOnePassVersion(false).encode(bcpgOutputStream);

        File inputFile = new File(inputFileName);
        PGPLiteralDataGenerator pgpLiteralDataGenerator = new PGPLiteralDataGenerator();
        //yes it will write to the same output file but with other crypting utils - the cortesy of BC architecture
        OutputStream literalOutputStream = pgpLiteralDataGenerator.open(bcpgOutputStream,
                PGPLiteralData.BINARY,
                inputFile);
        FileInputStream fileInputStream = new FileInputStream(inputFile);

        int ch;
        while((ch = fileInputStream.read()) >= 0) {
            literalOutputStream.write(ch);
            pgpSignatureGenerator.update((byte) ch);
        }
        pgpSignatureGenerator.generate().encode(bcpgOutputStream);

        fileInputStream.close();
        pgpLiteralDataGenerator.close();
        literalOutputStream.close();
        pgpCompressedDataGenerator.close();
        outputStream.close();
        bcpgOutputStream.close();

        return outputFile;
    }

    /**
     * @param pgpSecretKey - can be fetched by com.crypting.BCKeyPairUtil.readSecretKey
     * @param passPhrase - the phrase used on key exporting by com.crypting.BCKeyPairUtil#exportKeyPairLocally
     * @param asciiArmored - define if to apply binary-to-text encoding for signed string content -
     *      *              https://en.wikipedia.org/wiki/Binary-to-text_encoding
     * @throws IOException - according bouncycastle methods architecture
     * @throws PGPException - according bouncycastle methods architecture
     */
    public static String signString(PGPSecretKey pgpSecretKey,
                                    String inputString,
                                    String passPhrase,
                                    boolean asciiArmored) throws IOException, PGPException {
        Security.addProvider(new BouncyCastleProvider());

        PGPPrivateKey pgpPrivateKey = pgpSecretKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder()
                .setProvider("BC").build(passPhrase.toCharArray()));
        PGPSignatureGenerator pgpSignatureGenerator = new PGPSignatureGenerator(new JcaPGPContentSignerBuilder(
                pgpSecretKey.getPublicKey().getAlgorithm(),
                PGPUtil.SHA1)
                .setProvider("BC"));
        pgpSignatureGenerator.init(PGPSignature.BINARY_DOCUMENT, pgpPrivateKey);

        Iterator publicKeysIterator = pgpSecretKey.getPublicKey().getUserIDs();
        if(publicKeysIterator.hasNext()) {
            PGPSignatureSubpacketGenerator pgpSignatureSubpacketGenerator = new PGPSignatureSubpacketGenerator();
            pgpSignatureSubpacketGenerator.setSignerUserID(false, (String)publicKeysIterator.next());
            pgpSignatureGenerator.setHashedSubpackets(pgpSignatureSubpacketGenerator.generate());
        }

        PGPCompressedDataGenerator pgpCompressedDataGenerator = new PGPCompressedDataGenerator(PGPCompressedData.ZLIB);
        File temporaryOutputFile = new File(generateTemporaryFileName("outputfile"));
        OutputStream outputStream = new FileOutputStream(temporaryOutputFile);
        if(asciiArmored) {
            outputStream = new ArmoredOutputStream(outputStream);
        }
        BCPGOutputStream bcpgOutputStream = new BCPGOutputStream(pgpCompressedDataGenerator.open(outputStream));
        pgpSignatureGenerator.generateOnePassVersion(false).encode(bcpgOutputStream);

        File temporaryInputFile = new File(generateTemporaryFileName("inputfile"));
        temporaryInputFile.createNewFile();
        PrintWriter printWriter = new PrintWriter(temporaryInputFile);
        printWriter.println(inputString);
        printWriter.close();

        PGPLiteralDataGenerator pgpLiteralDataGenerator = new PGPLiteralDataGenerator();
        //yes it will write to the same output file but with other crypting utils - the cortesy of BC architecture
        OutputStream literalOutputStream = pgpLiteralDataGenerator.open(bcpgOutputStream,
                PGPLiteralData.BINARY,
                temporaryInputFile);
        FileInputStream fileInputStream = new FileInputStream(temporaryInputFile);

        int ch;
        while((ch = fileInputStream.read()) >= 0) {
            literalOutputStream.write(ch);
            pgpSignatureGenerator.update((byte) ch);
        }
        pgpSignatureGenerator.generate().encode(bcpgOutputStream);

        fileInputStream.close();
        pgpLiteralDataGenerator.close();
        literalOutputStream.close();
        pgpCompressedDataGenerator.close();
        outputStream.close();
        bcpgOutputStream.close();
        temporaryInputFile.delete();

        String result = new String(Files.readAllBytes(temporaryOutputFile.toPath()), "UTF-8");

        //TODO - currently not working by unknown reason
        temporaryOutputFile.delete();

        return result;
    }

    /**
     * @param signedFile file with signed file content
     * @param publicKeyFileName name of file with exported public key content
     * @return true if signature verification success
     * @throws Exception - according bouncycastle methods architecture
     */
    public static boolean verifyFile(File signedFile,
                                     String publicKeyFileName) throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        InputStream fileInputStream = PGPUtil.getDecoderStream(new FileInputStream(signedFile));

        JcaPGPObjectFactory jcaPGPObjectFactory = new JcaPGPObjectFactory(fileInputStream);
        PGPCompressedData pgpCompressedData = (PGPCompressedData) jcaPGPObjectFactory.nextObject();
        jcaPGPObjectFactory = new JcaPGPObjectFactory(pgpCompressedData.getDataStream());
        PGPOnePassSignatureList pgpOnePassSignatureList = (PGPOnePassSignatureList)jcaPGPObjectFactory.nextObject();
        PGPOnePassSignature pgpOnePassSignature = pgpOnePassSignatureList.get(0);
        PGPLiteralData pgpLiteralData = (PGPLiteralData) jcaPGPObjectFactory.nextObject();

        InputStream pgpLiteralDataInputStream = pgpLiteralData.getInputStream();
        InputStream keyInputStream = new FileInputStream(new File(publicKeyFileName));
        PGPPublicKeyRingCollection pgpPublicKeyRingCollection = new PGPPublicKeyRingCollection(
                PGPUtil.getDecoderStream(keyInputStream),
                new JcaKeyFingerprintCalculator());

        PGPPublicKey key = pgpPublicKeyRingCollection.getPublicKey(pgpOnePassSignature.getKeyID());
        File outputFile = new File(signedFile.getName()  + "_decoded");
        FileOutputStream outputStream = new FileOutputStream(outputFile);
        pgpOnePassSignature.init(new JcaPGPContentVerifierBuilderProvider().setProvider("BC"), key);

        int ch;
        while((ch = pgpLiteralDataInputStream.read()) >= 0) {
            pgpOnePassSignature.update((byte)ch);
            outputStream.write(ch);
        }

        boolean result = false;
        PGPSignatureList pgpSignatureList = (PGPSignatureList)jcaPGPObjectFactory.nextObject();
        if (pgpOnePassSignature.verify(pgpSignatureList.get(0))) {
            log.info("signature verified.");
            result = true;
        } else {
            log.info("signature verification failed.");
        }
        fileInputStream.close();
        pgpLiteralDataInputStream.close();
        outputStream.close();

        return result;
    }

    /**
     * @param signedFileName name of file with signed file content
     * @param publicKeyFileName name of file with exported public key content
     * @return outputFile created on file system with decrypted file content
     * @throws Exception - according bouncycastle methods architecture
     */
    public static File verifyFile(String signedFileName,
                                  String publicKeyFileName) throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        InputStream fileInputStream = PGPUtil.getDecoderStream(new FileInputStream(new File(signedFileName)));

        JcaPGPObjectFactory jcaPGPObjectFactory = new JcaPGPObjectFactory(fileInputStream);
        PGPCompressedData pgpCompressedData = (PGPCompressedData) jcaPGPObjectFactory.nextObject();
        jcaPGPObjectFactory = new JcaPGPObjectFactory(pgpCompressedData.getDataStream());
        PGPOnePassSignatureList pgpOnePassSignatureList = (PGPOnePassSignatureList)jcaPGPObjectFactory.nextObject();
        PGPOnePassSignature pgpOnePassSignature = pgpOnePassSignatureList.get(0);
        PGPLiteralData pgpLiteralData = (PGPLiteralData) jcaPGPObjectFactory.nextObject();

        InputStream pgpLiteralDataInputStream = pgpLiteralData.getInputStream();
        InputStream keyInputStream = new FileInputStream(new File(publicKeyFileName));
        PGPPublicKeyRingCollection pgpPublicKeyRingCollection = new PGPPublicKeyRingCollection(
                PGPUtil.getDecoderStream(keyInputStream),
                new JcaKeyFingerprintCalculator());

        PGPPublicKey key = pgpPublicKeyRingCollection.getPublicKey(pgpOnePassSignature.getKeyID());
        File outputFile = new File(signedFileName  + "_decoded");
        FileOutputStream outputStream = new FileOutputStream(outputFile);
        pgpOnePassSignature.init(new JcaPGPContentVerifierBuilderProvider().setProvider("BC"), key);

        int ch;
        while((ch = pgpLiteralDataInputStream.read()) >= 0) {
            pgpOnePassSignature.update((byte)ch);
            outputStream.write(ch);
        }

        PGPSignatureList pgpSignatureList = (PGPSignatureList)jcaPGPObjectFactory.nextObject();
        if (pgpOnePassSignature.verify(pgpSignatureList.get(0))) {
            log.info("signature verified.");
        } else {
            log.info("signature verification failed.");
        }
        fileInputStream.close();
        pgpLiteralDataInputStream.close();
        outputStream.close();

        return outputFile;
    }

    /**
     * @param inputString - signed string
     * @param publicKeyFileName name of file with exported public key content
     * @return
     * @throws Exception
     */
    public static String verifyString(String inputString,
                                      String publicKeyFileName) throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        File temporaryInputFile = new File(generateTemporaryFileName("inputfile"));
        temporaryInputFile.createNewFile();
        PrintWriter printWriter = new PrintWriter(temporaryInputFile);
        printWriter.println(inputString);
        printWriter.close();
        InputStream fileInputStream = PGPUtil.getDecoderStream(new FileInputStream(temporaryInputFile));

        JcaPGPObjectFactory jcaPGPObjectFactory = new JcaPGPObjectFactory(fileInputStream);
        PGPCompressedData pgpCompressedData = (PGPCompressedData) jcaPGPObjectFactory.nextObject();
        jcaPGPObjectFactory = new JcaPGPObjectFactory(pgpCompressedData.getDataStream());
        PGPOnePassSignatureList pgpOnePassSignatureList = (PGPOnePassSignatureList)jcaPGPObjectFactory.nextObject();
        PGPOnePassSignature pgpOnePassSignature = pgpOnePassSignatureList.get(0);
        PGPLiteralData pgpLiteralData = (PGPLiteralData) jcaPGPObjectFactory.nextObject();

        InputStream pgpLiteralDataInputStream = pgpLiteralData.getInputStream();
        InputStream keyInputStream = new FileInputStream(new File(publicKeyFileName));
        PGPPublicKeyRingCollection pgpPublicKeyRingCollection = new PGPPublicKeyRingCollection(
                PGPUtil.getDecoderStream(keyInputStream),
                new JcaKeyFingerprintCalculator());

        PGPPublicKey key = pgpPublicKeyRingCollection.getPublicKey(pgpOnePassSignature.getKeyID());
        File temporaryOutputFile = new File(generateTemporaryFileName("outputfile"));
        FileOutputStream outputStream = new FileOutputStream(temporaryOutputFile);
        pgpOnePassSignature.init(new JcaPGPContentVerifierBuilderProvider().setProvider("BC"), key);

        int ch;
        while((ch = pgpLiteralDataInputStream.read()) >= 0) {
            pgpOnePassSignature.update((byte)ch);
            outputStream.write(ch);
        }

        PGPSignatureList pgpSignatureList = (PGPSignatureList)jcaPGPObjectFactory.nextObject();
        if (pgpOnePassSignature.verify(pgpSignatureList.get(0))) {
            log.info("signature verified.");
        } else {
            log.info("signature verification failed.");
        }

        fileInputStream.close();
        pgpLiteralDataInputStream.close();
        outputStream.close();

        byte[] bytes = Files.readAllBytes(temporaryOutputFile.toPath());
        String result = new String(bytes,"UTF-8");
        temporaryInputFile.delete();
        temporaryOutputFile.delete();

        return result;
    }

    //TODO - remove
    private static void signFileWithStringDebugOutput(PGPSecretKey pgpSecretKey,
                                                     String inputFileName,
                                                     String outputFileName,
                                                     String passPhrase,
                                                     boolean asciiArmored) throws IOException, PGPException {
        Security.addProvider(new BouncyCastleProvider());

        PGPPrivateKey pgpPrivateKey = pgpSecretKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder()
                .setProvider("BC").build(passPhrase.toCharArray()));
        PGPSignatureGenerator pgpSignatureGenerator = new PGPSignatureGenerator(new JcaPGPContentSignerBuilder(
                pgpSecretKey.getPublicKey().getAlgorithm(),
                PGPUtil.SHA1)
                .setProvider("BC"));
        pgpSignatureGenerator.init(PGPSignature.BINARY_DOCUMENT, pgpPrivateKey);

        Iterator publicKeysIterator = pgpSecretKey.getPublicKey().getUserIDs();
        if(publicKeysIterator.hasNext()) {
            PGPSignatureSubpacketGenerator pgpSignatureSubpacketGenerator = new PGPSignatureSubpacketGenerator();
            pgpSignatureSubpacketGenerator.setSignerUserID(false, (String)publicKeysIterator.next());
            pgpSignatureGenerator.setHashedSubpackets(pgpSignatureSubpacketGenerator.generate());
        }

        PGPCompressedDataGenerator pgpCompressedDataGenerator = new PGPCompressedDataGenerator(PGPCompressedData.ZLIB);
        OutputStream outputStream = new FileOutputStream(new File(outputFileName));
        if(asciiArmored) {
            outputStream = new ArmoredOutputStream(outputStream);
        }
        BCPGOutputStream bcpgOutputStream = new BCPGOutputStream(pgpCompressedDataGenerator.open(outputStream));
        pgpSignatureGenerator.generateOnePassVersion(false).encode(bcpgOutputStream);

        File inputFile = new File(inputFileName);
        PGPLiteralDataGenerator pgpLiteralDataGenerator = new PGPLiteralDataGenerator();
        //yes it will write to the same output file but with other crypting utils - the cortesy of BC architecture
        OutputStream literalOutputStream = pgpLiteralDataGenerator.open(bcpgOutputStream,
                PGPLiteralData.BINARY,
                inputFile);
        FileInputStream fileInputStream = new FileInputStream(inputFile);


        //String debug output block start
        PGPSignatureGenerator pgpSignatureGenerator2 = new PGPSignatureGenerator(new JcaPGPContentSignerBuilder(
                pgpSecretKey.getPublicKey().getAlgorithm(),
                PGPUtil.SHA1)
                .setProvider("BC"));
        pgpSignatureGenerator2.init(PGPSignature.BINARY_DOCUMENT, pgpPrivateKey);
        PGPCompressedDataGenerator pgpCompressedDataGenerator2 = new PGPCompressedDataGenerator(PGPCompressedData.ZLIB);
        OutputStream outputStream2 = new ByteArrayOutputStream();
        BCPGOutputStream bcpgOutputStream2 = new BCPGOutputStream(pgpCompressedDataGenerator2.open(outputStream2));
        pgpSignatureGenerator2.generateOnePassVersion(false).encode(bcpgOutputStream2);
        PGPLiteralDataGenerator pgpLiteralDataGenerator2 = new PGPLiteralDataGenerator();
        OutputStream literalOutputStream2 = pgpLiteralDataGenerator2.open(bcpgOutputStream,
                PGPLiteralData.BINARY,
                inputFile);
        //String debug output block end

        int ch;
        while((ch = fileInputStream.read()) >= 0) {
            literalOutputStream.write(ch);
            literalOutputStream2.write(ch);
            pgpSignatureGenerator.update((byte) ch);
        }
        pgpSignatureGenerator.generate().encode(outputStream2);
        log.debug("outputStream2.toString() = " + outputStream2.toString());
        pgpSignatureGenerator.generate().encode(bcpgOutputStream);

        fileInputStream.close();
        pgpLiteralDataGenerator.close();
        literalOutputStream.close();
        pgpCompressedDataGenerator.close();
        outputStream.close();
    }


    /**
     * @param pgpSecretKey - can be fetched by com.crypting.BCKeyPairUtil.readSecretKey
     * @param inputString - inputString
     * @param passPhrase - the phrase used on key exporting by com.crypting.BCKeyPairUtil#exportKeyPairLocally
     * @return signed string
     * @throws IOException - according bouncycastle methods architecture
     * @throws PGPException - according bouncycastle methods architecture
     */
    //TODO - defined as private because it is not working properly now
    private static String signStringByteArrayVersion(PGPSecretKey pgpSecretKey,
                                                    String inputString,
                                                    String passPhrase) throws IOException, PGPException {
        Security.addProvider(new BouncyCastleProvider());

        PGPPrivateKey pgpPrivateKey = pgpSecretKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder()
                .setProvider("BC").build(passPhrase.toCharArray()));
        PGPSignatureGenerator pgpSignatureGenerator = new PGPSignatureGenerator(new JcaPGPContentSignerBuilder(
                pgpSecretKey.getPublicKey().getAlgorithm(),
                PGPUtil.SHA1)
                .setProvider("BC"));
        pgpSignatureGenerator.init(PGPSignature.BINARY_DOCUMENT, pgpPrivateKey);

        Iterator publicKeysIterator = pgpSecretKey.getPublicKey().getUserIDs();
        if(publicKeysIterator.hasNext()) {
            PGPSignatureSubpacketGenerator pgpSignatureSubpacketGenerator = new PGPSignatureSubpacketGenerator();
            pgpSignatureSubpacketGenerator.setSignerUserID(false, (String)publicKeysIterator.next());
            pgpSignatureGenerator.setHashedSubpackets(pgpSignatureSubpacketGenerator.generate());
        }

        PGPCompressedDataGenerator pgpCompressedDataGenerator = new PGPCompressedDataGenerator(PGPCompressedData.ZLIB);
        OutputStream outputStream = new ByteArrayOutputStream();
        BCPGOutputStream bcpgOutputStream = new BCPGOutputStream(pgpCompressedDataGenerator.open(outputStream));
        pgpSignatureGenerator.generateOnePassVersion(false).encode(bcpgOutputStream);

        CustomPGPLiteralDataGenerator pgpLiteralDataGenerator = new CustomPGPLiteralDataGenerator();
        //yes it will write to the same output file but with other crypting utils - the cortesy of BC architecture
        OutputStream literalOutputStream = pgpLiteralDataGenerator.customOpen(bcpgOutputStream,
                PGPLiteralData.BINARY,
                inputString.length());
        InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());

        int ch;
        while ((ch = inputStream.read()) >= 0) {
            literalOutputStream.write(ch);
            pgpSignatureGenerator.update((byte) ch);
        }
        //TODO probably that is the problem - bcpgOutputStream is used for signing files but its content can't be
        // output to string - it writes the content to the file so probably can be solved by BCPGOutputStream custom
        // implementation
        //pgpSignatureGenerator.generate().encode(bcpgOutputStream);
        pgpSignatureGenerator.generate().encode(outputStream);

        inputStream.close();
        pgpLiteralDataGenerator.close();
        literalOutputStream.close();
        pgpCompressedDataGenerator.close();

        String result = outputStream.toString();
        outputStream.close();
        return result;
    }

    //TODO - defined as private because it is not working properly now
    private static String verifyStringByteArrayVersion(String signedString,
                                                       String publicKeyFileName) throws Exception {
        Security.addProvider(new BouncyCastleProvider());
        //InputStream fileInputStream = PGPUtil.getDecoderStream(new FileInputStream(new File(signedFileName)));
        InputStream inputStream = new ByteArrayInputStream(signedString.getBytes());
        //fileInputStream = PGPUtil.getDecoderStream(fileInputStream);
        inputStream = PGPUtil.getDecoderStream(inputStream);

        //JcaPGPObjectFactory jcaPGPObjectFactory = new JcaPGPObjectFactory(fileInputStream);
        JcaPGPObjectFactory jcaPGPObjectFactory = new JcaPGPObjectFactory(inputStream);
        PGPCompressedData pgpCompressedData = (PGPCompressedData) jcaPGPObjectFactory.nextObject();
        jcaPGPObjectFactory = new JcaPGPObjectFactory(pgpCompressedData.getDataStream());
        PGPOnePassSignatureList pgpOnePassSignatureList = (PGPOnePassSignatureList)jcaPGPObjectFactory.nextObject();
        PGPOnePassSignature pgpOnePassSignature = pgpOnePassSignatureList.get(0);
        PGPLiteralData pgpLiteralData = (PGPLiteralData) jcaPGPObjectFactory.nextObject();

        InputStream pgpLiteralDataInputStream = pgpLiteralData.getInputStream();
        InputStream keyInputStream = new FileInputStream(new File(publicKeyFileName));
        PGPPublicKeyRingCollection pgpPublicKeyRingCollection = new PGPPublicKeyRingCollection(
                PGPUtil.getDecoderStream(keyInputStream),
                new JcaKeyFingerprintCalculator());

        PGPPublicKey key = pgpPublicKeyRingCollection.getPublicKey(pgpOnePassSignature.getKeyID());
        //FileOutputStream outputStream = new FileOutputStream(signedFileName  + "_decoded");
        OutputStream outputStream = new ByteArrayOutputStream();
        pgpOnePassSignature.init(new JcaPGPContentVerifierBuilderProvider().setProvider("BC"), key);

        int ch;
        while((ch = pgpLiteralDataInputStream.read()) >= 0) {
            pgpOnePassSignature.update((byte)ch);
            outputStream.write(ch);
        }

        String result = outputStream.toString();

        PGPSignatureList pgpSignatureList = (PGPSignatureList)jcaPGPObjectFactory.nextObject();
        if(pgpOnePassSignature.verify(pgpSignatureList.get(0))) {
            log.info("signature verified.");
        } else {
            log.info("signature verification failed.");
        }

        inputStream.close();
        keyInputStream.close();
        pgpLiteralDataInputStream.close();
        outputStream.close();

        return result;
    }

}
