package com.crypting;

import org.apache.log4j.Logger;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.junit.Ignore;
import org.junit.Test;

import java.io.File;

//TODO - add @Before and @After methods for creating files on the beginning of tests and removing after completion
public class BCDigitalSignatureUtilTest {
    final static Logger log = Logger.getLogger(BCDigitalSignatureUtilTest.class);

    //ignored by default because it produce files on filesystem
    @Ignore
    @Test
    public void signFile() throws Exception {
        PGPSecretKey secretKey = BCKeyPairUtil.readSecretKey("unitTestKey1_1587371700506_secret.asc", true);
        String keyNameStart = "unitTestKey1";
        BCDigitalSignatureUtil.signFile(secretKey,
                "test_file.txt",
                "signed_file.txt",
                keyNameStart + "passPhrase",
                true);
    }

    //ignored by default because it produce files on filesystem
    @Ignore
    @Test
    public void verifyFile() throws Exception {
        BCDigitalSignatureUtil.verifyFile("signed_file.txt",
                "unitTestKey1_1587371700506_pub.asc");
    }

    //ignored by default because it produce files on filesystem
    //@Ignore
    @Test
    public void signAndVerifyFile() throws Exception {
        PGPSecretKey secretKey = BCKeyPairUtil.readSecretKey("unitTestKey1_1587371700506_secret.asc", true);
        String keyNameStart = "unitTestKey1";
        File signedFile = BCDigitalSignatureUtil.signFile(secretKey,
                "test_file.txt",
                "signed_file.txt",
                keyNameStart + "passPhrase",
                true);
        BCDigitalSignatureUtil.verifyFile(signedFile,
                "unitTestKey1_1587371700506_pub.asc");
    }

    @Test
    public void signAndVerifyString() throws Exception {
        PGPSecretKey secretKey = BCKeyPairUtil.readSecretKey("unitTestKey1_1587371700506_secret.asc", true);
        String keyNameStart = "unitTestKey1";
        String inputString = "hello world";
        log.info("inputString = " + inputString);

        String signedString = BCDigitalSignatureUtil.signString(secretKey,
                inputString,
                keyNameStart + "passPhrase",
                true);
        log.info("signedString = " + signedString);

        String verifiedString = BCDigitalSignatureUtil.verifyString(signedString,
                "unitTestKey1_1587371700506_pub.asc");
        log.info("verifiedString = " + verifiedString);

        BCDigitalSignatureUtil.deleteTemporaryFiles();
    }

}
